/*
	Sends GOKZ server records to Discord webhook
	Requires:
	- GOKZ (https://bitbucket.org/kztimerglobalteam/gokz)
	- SteamWorks (https://forums.alliedmods.net/showthread.php?t=229556)
*/

// ====================== DEFINITIONS ======================== //

//#define DEBUG

#define MAX_HOSTNAME 64
#define MAX_WEBHOOK_URL 256
#define MAX_JSON_LENGTH 2048

#define WEBHOOK_KEY_CONFIG "cfg/sourcemod/GOKZ-Records-Webhook-key.cfg"
#define WEBHOOK_DATA_CONFIG "cfg/sourcemod/GOKZ-Records-Webhook-data.cfg"

// =========================================================== //

#include <json>
#include <SteamWorks>

#include <gokz/core>
#include <gokz/localdb>
#include <gokz/localranks>

// ====================== FORMATTING ========================= //

#pragma dynamic 131072
#pragma newdecls required

// ====================== VARIABLES ========================== //

ConVar gCV_hostName = null;
char gC_webHookUrl[MAX_WEBHOOK_URL];
char gC_webHookData[MAX_JSON_LENGTH];

char gC_hostName[MAX_HOSTNAME];
char gC_currentMap[PLATFORM_MAX_PATH];

// ====================== PLUGIN INFO ======================== //

public Plugin myinfo = 
{
	name = "GOKZ-Records-Webhook",
	author = "Sikari",
	description = "Sends GOKZ server records to Discord Webhook",
	version = "1.0.0",
	url = "",
};

// ======================= MAIN CODE ========================= //

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{	
	gCV_hostName = FindConVar("hostname");
	gCV_hostName.AddChangeHook(ConVarHook);
}

public void OnPluginStart()
{
	File dataFile = OpenFile(WEBHOOK_DATA_CONFIG, "a+");
	dataFile.ReadString(gC_webHookData, sizeof(gC_webHookData), sizeof(gC_webHookData));
	dataFile.Close();
	
	File webHookFile = OpenFile(WEBHOOK_KEY_CONFIG, "a+");
	webHookFile.ReadLine(gC_webHookUrl, sizeof(gC_webHookUrl));
	webHookFile.Close();
}

public void OnMapStart()
{
	GetCurrentMap(gC_currentMap, sizeof(gC_currentMap));
	GetMapDisplayName(gC_currentMap, gC_currentMap, sizeof(gC_currentMap));
}

public void OnConfigsExecuted()
{
	gCV_hostName.GetString(gC_hostName, sizeof(gC_hostName));
}

public void ConVarHook(ConVar cvar, char[] oldValue, char[] newValue)
{
	if (!StrEqual(oldValue, newValue))
	{
		if (cvar == gCV_hostName)
		{
			strcopy(gC_hostName, sizeof(gC_hostName), newValue);
		}
	}
}

// =========================================================== //

void PostRecord(int client, int steamid, int course, int mode, float runTime, int teleports)
{
	RunVariableChecks(client, steamid, runTime, mode, course, teleports, gC_webHookData, sizeof(gC_webHookData));

	JSON_Object obj = new JSON_Object();
	obj.Decode(gC_webHookData);
	
	MakeRequest(obj);
}

void RunVariableChecks(int client, int steamid, float time, int mode, int course, int teleports, char[] buf, int maxlength)
{
	char szCourse[16] = "Main Course";
	if (course > 0) Format(szCourse, sizeof(szCourse), "Bonus %d", course);
	
	char szName[MAX_NAME_LENGTH];
	GetClientName(client, szName, sizeof(szName));
	
	char szSteamId32[64];
	IntToString(steamid, szSteamId32, sizeof(szSteamId32));
	
	ReplaceString(buf, maxlength, "{map}", gC_currentMap);
	ReplaceString(buf, maxlength, "{mode}", gC_ModeNames[mode]);
	ReplaceString(buf, maxlength, "{type}", teleports > 0 ? "NUB" : "PRO");
	ReplaceString(buf, maxlength, "{time}", GOKZ_FormatTime(time));
	ReplaceString(buf, maxlength, "{course}", szCourse);
	ReplaceString(buf, maxlength, "{hostname}", gC_hostName);
	ReplaceString(buf, maxlength, "{steamid32}", szSteamId32);
	ReplaceString(buf, maxlength, "{playername}", szName);
}

// =========================================================== //

public void MakeRequest(JSON_Object obj)
{
	Handle request = SteamWorks_CreateHTTPRequest(k_EHTTPMethodPOST, gC_webHookUrl);
	
	if (request == null)
	{
		delete request;
		return;
	}

	char json[MAX_JSON_LENGTH];
	obj.Encode(json, sizeof(json));

	SteamWorks_SetHTTPRequestRawPostBody(request, "application/json", json, strlen(json));
	SteamWorks_SetHTTPCallbacks(request, OnRequestCompleted);
	SteamWorks_SendHTTPRequest(request);
	
	// Cleanup
	obj.Cleanup();
	delete obj;
}

public int OnRequestCompleted(Handle request, bool failure, bool requestSuccessful, EHTTPStatusCode statusCode)
{
	delete request;
	
	#if defined DEBUG
	LogMessage("Webhook request sent! - Status: %d - Request Successful: %d - Failure: %d", statusCode, requestSuccessful, failure);
	#endif
}

// ======================= LISTENERS ========================= //

public void GOKZ_LR_OnTimeProcessed(
	int client,
	int steamID,
	int mapID,
	int course,
	int mode,
	int style,
	float runTime,
	int teleportsUsed,
	bool firstTime,
	float pbDiff,
	int rank,
	int maxRank,
	bool firstTimePro,
	float pbDiffPro,
	int rankPro,
	int maxRankPro)
{
	if ((rank == 1 && !GOKZ_LR_GetRecordMissed(client, TimeType_Nub)) || (rankPro == 1 && !GOKZ_LR_GetRecordMissed(client, TimeType_Pro)))
	{
		PostRecord(client, steamID, course, mode, runTime, teleportsUsed);
	}
}

// =========================================================== //