#define DATA_FILE "cfg/sourcemod/whitelister.cfg"

enum UserLevel
{
	UL_ROOT = 0,
	UL_WHITELIST
};

methodmap WhitelistUser < StringMap
{
	public WhitelistUser(char[] display, char[] steamid, char[] userLevel)
	{
		StringMap user = new StringMap();
		user.SetString("steamid", steamid);
		user.SetString("display", display);
		user.SetString("ulstring", userLevel);
		
		if (StrEqual(userLevel, "root"))
		{
			user.SetValue("userlevel", UL_ROOT);
		}
		else if (StrEqual(userLevel, "whitelist"))
		{
			user.SetValue("userlevel", UL_WHITELIST);
		}
		
		return view_as<WhitelistUser>(user);
	}
	
	property UserLevel UserLevel
	{
		public get()
		{
			UserLevel level;
			this.GetValue("userlevel", level);
			return level;
		}
	}
	
	public void GetSteamId(char[] buffer, int maxlength)
	{
		this.GetString("steamid", buffer, maxlength);
	}

	public void GetDisplayName(char[] buffer, int maxlength)
	{
		this.GetString("display", buffer, maxlength);
	}
	
	public void GetUserLevelStr(char[] buffer, int maxlength)
	{
		this.GetString("ulstring", buffer, maxlength);
	}
}

StringMap g_WhitelistData = null;
ConVar gCV_EveryoneAllowed = null;
ConVar gCV_WhitelistAllowed = null;

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	gCV_EveryoneAllowed = CreateConVar("Whitelister_EveryoneAllowed", "0", "Allow everyone to join the server", _, true, 0.0, true, 1.0);
	gCV_WhitelistAllowed = CreateConVar("Whitelister_WhitelistAllowed", "1", "Allow whitelist users to join the server", _, true, 0.0, true, 1.0);

	RegConsoleCmd("sm_wl_dump", Command_Dump);
	RegAdminCmd("sm_wl_reload", Command_Reload, ADMFLAG_ROOT);
	RegAdminCmd("sm_wl_adduser", Command_AddUser, ADMFLAG_ROOT);
	RegAdminCmd("sm_wl_removeuser", Command_RemoveUser, ADMFLAG_ROOT);
}

public void OnPluginStart()
{
	g_WhitelistData = ReadConfig(DATA_FILE);
	AutoExecConfig(true, "whitelister-config");
}

public void OnClientAuthorized(int client, const char[] auth)
{
	if (IsFakeClient(client))
	{
		return;
	}
	
	char steamId[32];
	if (GetClientAuthId(client, AuthId_Steam2, steamId, sizeof(steamId)))
	{
		WhitelistUser wlUser = null;
		g_WhitelistData.GetValue(steamId, wlUser);
		
		if (wlUser != null)
		{
			if (wlUser.UserLevel == UL_WHITELIST)
			{
				if (!gCV_EveryoneAllowed.BoolValue && !gCV_WhitelistAllowed.BoolValue)
				{
					KickClient(client, "Whitelist users are not allowed at this time!");
				}
			}
		}
		else
		{
			if (!gCV_EveryoneAllowed.BoolValue)
			{
				KickClient(client, "You're not allowed on this server!");
			}
		}
	}
	else
	{
		KickClient(client, "Could not authorize with Steam, please try again later");
	}
}

// TODO: Add client validity checks for the commands
public Action Command_Dump(int client, int args)
{
	StringMapSnapshot snap = g_WhitelistData.Snapshot();
	
	for (int i = 0; i < snap.Length; i++)
	{
		char key[32];
		snap.GetKey(i, key, sizeof(key));
		
		WhitelistUser wlUser = null;
		g_WhitelistData.GetValue(key, wlUser);
		
		if (wlUser != null)
		{
			char steamId[32];
			wlUser.GetSteamId(steamId, sizeof(steamId));
			
			char displayName[128];
			wlUser.GetDisplayName(displayName, sizeof(displayName));
			
			char userLevelStr[24];
			wlUser.GetUserLevelStr(userLevelStr, sizeof(userLevelStr));

			ReplyToCommand(client, "=> SteamId: %s", steamId);
			ReplyToCommand(client, "==> Display Name: %s", displayName);
			ReplyToCommand(client, "===> User Level: %s", userLevelStr);
			ReplyToCommand(client, "");
		}
	}
	
	delete snap;
}

public Action Command_Reload(int client, int args)
{
	g_WhitelistData = ReadConfig(DATA_FILE);
}

public Action Command_AddUser(int client, int args)
{
	char steamId[32];
	GetCmdArg(1, steamId, sizeof(steamId));
	
	char displayName[128];
	GetCmdArg(2, displayName, sizeof(displayName));

	char userLevel[24];
	GetCmdArg(3, userLevel, sizeof(userLevel));
	
	WhitelistUser wlUser = null;
	g_WhitelistData.GetValue(steamId, wlUser);
	
	if (strlen(displayName) <= 0)
	{
		displayName = "?";
		ReplyToCommand(client, "No display name specified, defaulted to '?'");
	}
	
	if (!StrEqual(userLevel, "root") && !StrEqual(userLevel, "whitelist"))
	{
		userLevel = "whitelist";
		ReplyToCommand(client, "Invalid userlevel specified, defaulted to whitelist");
	}
	
	if (wlUser == null)
	{
		if (EditKeyValuesFile(DATA_FILE, userLevel, steamId, displayName, false))
		{
			g_WhitelistData.SetValue(steamId, new WhitelistUser(displayName, steamId, userLevel));
			ReplyToCommand(client, "Added %s (%s) as %s user to the whitelist", steamId, displayName, userLevel);
		}
		else
		{
			ReplyToCommand(client, "Failed adding %s (%s) as %s user to the whitelist", steamId, displayName, userLevel);
		}
	}
}

public Action Command_RemoveUser(int client, int args)
{
	// Support for deleting by display name?
	char steamId[32];
	GetCmdArg(1, steamId, sizeof(steamId));
	
	WhitelistUser wlUser = null;
	g_WhitelistData.GetValue(steamId, wlUser);
	
	if (wlUser != null)
	{
		char userLevel[24];
		wlUser.GetUserLevelStr(userLevel, sizeof(userLevel));

		if (EditKeyValuesFile(DATA_FILE, userLevel, steamId, "", true))
		{
			ReplyToCommand(client, "Removed %s from the whitelist", steamId);
		}
		else
		{
			ReplyToCommand(client, "Failed removing %s from the whitelist", steamId);
		}

		delete wlUser;
		g_WhitelistData.Remove(steamId);
	}
}

static StringMap ReadConfig(char[] configFile)
{
	KeyValues config = new KeyValues("Whitelister");
	
	if (!FileExists(configFile))
	{
		config.ExportToFile(configFile);
	}
	
	if (!config.ImportFromFile(configFile))
	{
		SetFailState("Failed parsing %s as KeyValues!", configFile);
	}
	
	StringMap data = new StringMap();
	
	do
	{
		char section[24];
		config.GetSectionName(section, sizeof(section));
		
		if (StrEqual(section, "root") || StrEqual(section, "whitelist"))
		{
			while (config.GotoFirstSubKey(false) || config.GotoNextKey(false))
			{
				char steamId[32];
				config.GetSectionName(steamId, sizeof(steamId));
				
				char display[128];
				config.GetString(NULL_STRING, display, sizeof(display));

				data.SetValue(steamId, new WhitelistUser(display, steamId, section));
			}
			
			config.Rewind();
			config.JumpToKey(section);
		}
	} while (config.GotoNextKey() || config.GotoFirstSubKey());
	
	delete config;
	return data;
}

static bool EditKeyValuesFile(const char[] file, char[] section, char[] key, char[] value, bool remove = false)
{
	KeyValues kv = new KeyValues("Whitelister");

	if (!kv.ImportFromFile(file))
	{
		SetFailState("Failed reading %s as KeyValues!", file);
	}
	
	bool result = false;
	kv.JumpToKey(section, true);

	if (remove)
	{
		kv.DeleteKey(key);
	}
	else
	{
		kv.SetString(key, value);
	}

	kv.Rewind();
	result = kv.ExportToFile(file);

	delete kv;
	return result;
}