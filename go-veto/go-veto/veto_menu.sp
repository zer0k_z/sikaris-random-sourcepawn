// =========================================================== //

static Menu H_VetoMenu = null;

// =========================================================== //

void InitVetoMenu()
{
	H_VetoMenu = new Menu(VetoMenu_Handler);
}

void ResetVetoMenu()
{
	if (H_VetoMenu != null)
	{
		H_VetoMenu.Cancel();
	}
}

void DisplayVetoMenu(int client)
{
	H_VetoMenu.RemoveAllItems();
	char title[128] = GOVETO_TAG_RAW;

	for (int i = 0; i < g_Veto.Players.Length; i++)
	{
		VetoPlayer player = g_Veto.Players.Get(i);

		if (i == 0)
		{
			Format(title, sizeof(title), "%s %N", title, player.Client);
		}
		else
		{
			Format(title, sizeof(title), "%s vs %N", title, player.Client);
		}
	}
	
	Format(title, sizeof(title), "%s\nIt's your turn to %s!", title, g_Veto.Phase == 1 ? "pick" : "ban");

	for (int i = 0; i < g_Veto.Maps.Length; i++)
	{
		char map[PLATFORM_MAX_PATH];
		g_Veto.Maps.GetString(i, map, sizeof(map));

		char index[12];
		IntToString(i, index, sizeof(index));
		
		H_VetoMenu.AddItem(index, map);
	}
	
	H_VetoMenu.SetTitle(title);
	H_VetoMenu.ExitButton = false;
	H_VetoMenu.Display(client, MENU_TIME_FOREVER);
}

// =========================================================== //

public int VetoMenu_Handler(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_Select)
	{
		char itemInfo[12];
		menu.GetItem(param2, itemInfo, sizeof(itemInfo));

		int selection = StringToInt(itemInfo);

		char map[PLATFORM_MAX_PATH];
		g_Veto.Maps.GetString(selection, map, sizeof(map));

		int mapIndex = g_Veto.Maps.FindString(map);
		g_Veto.Maps.Erase(mapIndex);

		VoteType type = g_Veto.Phase == 1 ? Vote_Pick : Vote_Ban;
		g_Veto.Votes.PushVote(g_Veto.Voter, map, type);

		VetoAnnounce(g_Veto.Voter, map, type);
		VetoProceed();
	}
}

// =========================================================== //