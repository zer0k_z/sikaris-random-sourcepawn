// =========================================================== //

void VetoStart(int bestOf, int mapPool)
{
	switch (mapPool)
	{
		case 9:
		{
			switch (bestOf)
			{
				case 3:
				{
					g_Veto.Bans1 = 2;
					g_Veto.Picks = 2;
					g_Veto.Bans2 = 4;
				}
				case 5:
				{
					g_Veto.Bans1 = 2;
					g_Veto.Picks = 4;
					g_Veto.Bans2 = 2;
				}
			}
		}
		case 7:
		{
			switch (bestOf)
			{
				case 3:
				{
					g_Veto.Bans1 = 2;
					g_Veto.Picks = 2;
					g_Veto.Bans2 = 2;
				}
				case 5:
				{
					g_Veto.Bans1 = 2;
					g_Veto.Picks = 4;
					g_Veto.Bans2 = 0;
				}
			}
		}
	}	

	g_Veto.Active = true;
	GoVeto_PrintAll("\x05BO%d VETO Starting!\x01", bestOf);

	for (int i = 0; i < g_Veto.Players.Length; i++)
	{
		VetoPlayer player = g_Veto.Players.Get(i);
		
		char name[MAX_NAME_LENGTH];
		player.GetName(name, sizeof(name));
		
		GoVeto_PrintAll("Participant %d: \x08%s\x01", (i + 1), name);
	}
}

void VetoReset()
{
	g_Veto.Reset();
	ResetVetoMenu();
}

void VetoProceed()
{
	g_Veto.Phase = GetVetoPhase();
	g_Veto.Voter = GetNextVoter();
	int total = g_Veto.Bans1 + g_Veto.Picks + g_Veto.Bans2;

	if (GetTotalSelectionsCount() >= total)
	{
		FinishVeto();
	}
	else
	{
		DisplayVetoMenu(g_Veto.Voter.Client);
	}
}

void VetoAnnounce(VetoPlayer player, char[] map, VoteType type)
{
	char playerName[MAX_NAME_LENGTH];
	player.GetName(playerName, sizeof(playerName));

	switch (type)
	{
		case Vote_Ban:
		{
			GoVeto_PrintAll("\x08%s\x01 \x07banned\x01 \x03%s\x01", playerName, map);
		}
		case Vote_Pick:
		{
			GoVeto_PrintAll("\x08%s\x01 \x04picked\x01 \x03%s\x01", playerName, map);
		}
		case Vote_Decider:
		{
			GoVeto_PrintAll("\x04%s\x01 was left as the decider!", map);
		}
	}
}

// =========================================================== //

static int GetTotalSelectionsCount()
{
	return g_Veto.MapPool.Length - g_Veto.Maps.Length;
}

static VetoPlayer GetNextVoter()
{
	int current = g_Veto.Players.FindValue(g_Veto.Voter);

	int nextIndex = (current + 1);
	if (nextIndex >= g_Veto.Players.Length)
	{
		nextIndex = 0;
	}

	return g_Veto.Players.Get(nextIndex);
}

static int GetVetoPhase()
{
	int bans1 = g_Veto.Bans1;
	int picks = bans1 + g_Veto.Picks;
	int bans2 = picks + g_Veto.Bans2;
	int totalSelections = GetTotalSelectionsCount();
	
	if (totalSelections < bans1)
	{
		return 0;
	}
	else
	{
		if (totalSelections < picks)
		{
			return 1;
		}
		else if (totalSelections < bans2)
		{
			return 2;
		}
	}

	// uh oh!!
	return -1;
}

static void FinishVeto()
{
	char lastMap[PLATFORM_MAX_PATH];
	g_Veto.Maps.GetString(0, lastMap, sizeof(lastMap));

	g_Veto.Maps.Clear();
	g_Veto.Active = false;
	
	VetoPlayer server = new VetoPlayer(0);
	
	VetoAnnounce(server, lastMap, Vote_Decider);
	g_Veto.Votes.PushVote(server, lastMap, Vote_Decider);
	
	int mapNum = 0;
	GoVeto_PrintAll("VETO ended! Results are:");
	for (int i = 0; i < g_Veto.Votes.Length; i++)
	{
		VetoVote vote = g_Veto.Votes.Get(i);
		if (vote.Type == Vote_Pick || vote.Type == Vote_Decider)
		{
			char map[PLATFORM_MAX_PATH];
			vote.GetMap(map, sizeof(map));
			GoVeto_PrintAll("Map %d: \x05%s\x01", ++mapNum, map);
		}
	}
}

// =========================================================== //