// ====================== DEFINITIONS ======================== //

#define VETO_ADMIN_FLAGBITS ADMFLAG_RCON

#define GOVETO_TAG_RAW "[GO-VETO]"
#define GOVETO_TAG_COLOR "[\x0CGO-VETO\x01]"

// =========================================================== //

#include <go-veto>

// ====================== FORMATTING ========================= //

#pragma newdecls required

// ====================== VARIABLES ========================== //

VetoConfig g_Veto = null;

// ======================= INCLUDES ========================== //

#include "go-veto/veto_menu.sp"
#include "go-veto/veto_logic.sp"

// ====================== PLUGIN INFO ======================== //

public Plugin myinfo = 
{
	name = GOVETO_NAME,
	author = GOVETO_AUTHOR,
	description = "",
	version = GOVETO_VERSION,
	url = GOVETO_URL
};

// ======================= MAIN CODE ========================= //

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	LoadTranslations("common.phrases");

	RegConsoleCmd("sm_goveto_abort", Command_VetoAbort);
	RegConsoleCmd("sm_goveto_start_bo3", Command_VetoStart);
	RegConsoleCmd("sm_goveto_start_bo5", Command_VetoStart);

	RegConsoleCmd("sm_goveto_mappool", Command_VetoMapPool);
	RegConsoleCmd("sm_goveto_results", Command_VetoResults);
}

public void OnPluginStart()
{
	InitVetoMenu();
	g_Veto = new VetoConfig();

	ReadMapList(g_Veto.MapPool, _, _, MAPLIST_FLAG_CLEARARRAY);

	// Limit to 9 for now...
	if (g_Veto.MapPool.Length > 9)
	{
		g_Veto.MapPool.Resize(9);
	}
}

public Action Command_VetoAbort(int client, int args)
{
	if (!IsValidClient(client))
	{
		return Plugin_Handled;
	}

	if (!IsVetoAdmin(client))
	{
		GoVeto_Print(client, "This command is only usable by the administrators!");
		return Plugin_Handled;
	}

	if (!g_Veto.Active)
	{
		GoVeto_Print(client, "Theres no ongoing veto!");
		return Plugin_Handled;
	}

	VetoReset();

	GoVeto_Print(client, "Cancelled outgoing veto!");
	return Plugin_Handled;
}

public Action Command_VetoStart(int client, int args)
{
	if (!IsValidClient(client))
	{
		return Plugin_Handled;
	}

	if (!IsVetoAdmin(client))
	{
		GoVeto_Print(client, "This command is only usable by the administrators!");
		return Plugin_Handled;
	}

	if (g_Veto.Active)
	{
		GoVeto_Print(client, "There is already an ongoing veto!");
		return Plugin_Handled;
	}

	if (args <= 1)
	{
		GoVeto_Print(client, "Specify 2 players in this veto!");
		return Plugin_Handled;
	}

	char arg1[MAX_NAME_LENGTH];
	GetCmdArg(1, arg1, sizeof(arg1));

	char arg2[MAX_NAME_LENGTH];
	GetCmdArg(2, arg2, sizeof(arg2));

	int player1 = FindTarget(client, arg1, true, false);
	int player2 = FindTarget(client, arg2, true, false);

	if (player1 == -1 || player2 == -1)
	{
		GoVeto_Print(client, "Failed to find participants!");
		return Plugin_Handled;
	}

	if (player1 == player2)
	{
		GoVeto_Print(client, "Participants cannot be the same player!");
		return Plugin_Handled;
	}

	if (g_Veto.MapPool.Length != 9 || g_Veto.MapPool.Length != 7)
	{
		GoVeto_Print(client, "This plugin only supports 7 or 9 maps as of now!");
		return Plugin_Handled;
	}

	char cmd[128];
	GetCmdArg(0, cmd, sizeof(cmd));
	int bestOf = StringToInt(cmd[strlen(cmd) - 1]);

	VetoReset();
	g_Veto.Init(player1, player2);

	VetoStart(bestOf,g_Veto.MapPool.Length);
	DisplayVetoMenu(player1);
	return Plugin_Handled;
}

public Action Command_VetoMapPool(int client, int args)
{
	if (!IsValidClient(client))
	{
		return Plugin_Handled;
	}

	GoVeto_Print(client, "The map pool is:");
	for (int i = 0; i < g_Veto.MapPool.Length; i++)
	{
		char map[PLATFORM_MAX_PATH];
		g_Veto.MapPool.GetString(i, map, sizeof(map));
		GoVeto_Print(client, "\x05%s\x01", map);
	}

	return Plugin_Handled;
}

public Action Command_VetoResults(int client, int args)
{
	if (!IsValidClient(client))
	{
		return Plugin_Handled;
	}

	if (g_Veto.Votes.Length <= 0)
	{
		return Plugin_Handled;
	}
	
	GoVeto_Print(client, "The following maps have been picked:");
	for (int i = 0; i < g_Veto.Votes.Length; i++)
	{
		VetoVote vote = g_Veto.Votes.Get(i);
		if (vote.Type == Vote_Pick || vote.Type == Vote_Decider)
		{
			char map[PLATFORM_MAX_PATH];
			vote.GetMap(map, sizeof(map));
			
			char player[MAX_NAME_LENGTH];
			vote.Player.GetName(player, sizeof(player));
			
			GoVeto_Print(client, "\x04%s\x01 (Picked by %s)", map, player);
		}
	}

	GoVeto_Print(client, "The following maps have been banned:");
	for (int i = 0; i < g_Veto.Votes.Length; i++)
	{
		VetoVote vote = g_Veto.Votes.Get(i);
		if (vote.Type == Vote_Ban)
		{
			char map[PLATFORM_MAX_PATH];
			vote.GetMap(map, sizeof(map));
			
			char player[MAX_NAME_LENGTH];
			vote.Player.GetName(player, sizeof(player));

			GoVeto_Print(client, "\x07%s\x01 (Banned by %s)", map, player);
		}
	}

	return Plugin_Handled;
}

// =========================================================== //

static bool IsVetoAdmin(int client)
{
	return (CheckCommandAccess(client, "", VETO_ADMIN_FLAGBITS, true));
}

static bool IsValidClient(int client)
{
	return (client >= 1 && client <= MaxClients && IsClientInGame(client));
}

// =========================================================== //

void GoVeto_Print(int client, const char[] format, any ...)
{
	char buffer[512];
	VFormat(buffer, sizeof(buffer), format, 3);
	
	PrintToChat(client, "%s %s", GOVETO_TAG_COLOR, buffer);
}

void GoVeto_PrintAll(const char[] format, any ...)
{
	char buffer[512];
	VFormat(buffer, sizeof(buffer), format, 2);
	
	for (int i = 1; i <= MaxClients; i++)
	{
		if (!IsClientConnected(i))
		{
			continue;
		}
		
		if (!IsClientInGame(i))
		{
			continue;
		}

		GoVeto_Print(i, buffer);
	}
}

// =========================================================== //