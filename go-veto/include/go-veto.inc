// ================== DOUBLE INCLUDE ========================= //

#if defined _GoVeto_included_
#endinput
#endif
#define _GoVeto_included_

// ======================= DEFINITIONS ======================= //

#define GOVETO_NAME "GO-VETO"
#define GOVETO_AUTHOR "Sikari"
#define GOVETO_VERSION "1.0.0"
#define GOVETO_URL ""

// =========================================================== //

#include <go-veto/VetoPlayer>
#include <go-veto/VetoVote>
#include <go-veto/VetoConfig>

// =========================================================== //