// ================== DOUBLE INCLUDE ========================= //

#if defined _GoVeto_VetoConfig_included_
#endinput
#endif
#define _GoVeto_VetoConfig_included_

// =========================================================== //

methodmap VetoConfig < StringMap
{
	public VetoConfig()
	{
		StringMap config = new StringMap();
		config.SetValue("votes", new VetoVotes());
		config.SetValue("mappool", new ArrayList(64));
		config.SetValue("players", new VetoPlayers());
		return view_as<VetoConfig>(config);
	}
	
	property bool Active
	{
		public get()
		{
			bool active = false;
			this.GetValue("active", active);
			return active;
		}
		public set(bool active)
		{
			this.SetValue("active", active);
		}
	}
	
	property ArrayList Maps
	{
		public get()
		{
			ArrayList maps = null;
			this.GetValue("maps", maps);
			return maps;
		}
		public set(ArrayList maps)
		{
			this.SetValue("maps", maps);
		}
	}

	property ArrayList MapPool
	{
		public get()
		{
			ArrayList mapPool = null;
			this.GetValue("mappool", mapPool);
			return mapPool;
		}
	}

	property VetoPlayers Players
	{
		public get()
		{
			VetoPlayers players = null;
			this.GetValue("players", players);
			return players;
		}
	}

	property VetoVotes Votes
	{
		public get()
		{
			VetoVotes votes = null;
			this.GetValue("votes", votes);
			return votes;
		}
	}

	property VetoPlayer Voter
	{
		public get()
		{
			VetoPlayer voter = null;
			this.GetValue("voter", voter);
			return voter;
		}
		public set(VetoPlayer voter)
		{
			this.SetValue("voter", voter);
		}
	}

	property int Phase
	{
		public get()
		{
			int phase = -1;
			this.GetValue("phase", phase);
			return phase;
		}
		public set(int phase)
		{
			this.SetValue("phase", phase);
		}
	}

	property int Bans1
	{
		public get()
		{
			int bans1 = -1;
			this.GetValue("bans1", bans1);
			return bans1;
		}
		public set(int bans1)
		{
			this.SetValue("bans1", bans1);
		}
	}
	
	property int Bans2
	{
		public get()
		{
			int bans2 = -1;
			this.GetValue("bans2", bans2);
			return bans2;
		}
		public set(int bans2)
		{
			this.SetValue("bans2", bans2);
		}
	}
	
	property int Picks
	{
		public get()
		{
			int picks = 0;
			this.GetValue("picks", picks);
			return picks;
		}
		public set(int picks)
		{
			this.SetValue("picks", picks);
		}
	}
	
	public void Init(int player1, int player2)
	{
		this.Players.PushClient(player1);
		this.Players.PushClient(player2);
		this.Maps = this.MapPool.Clone();
		this.Voter = this.Players.Get(0);
	}
	
	public void Reset()
	{
		this.Phase = 0;
		this.Active = false;
		
		if (this.Maps != null)
		{
			this.Maps.Clear();
		}
		if (this.Votes != null)
		{
			this.Votes.Reset();
		}		
		if (this.Players != null)
		{
			this.Players.Reset();
		}
	}
}

// =========================================================== //