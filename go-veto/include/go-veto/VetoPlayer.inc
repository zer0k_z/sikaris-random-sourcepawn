// ================== DOUBLE INCLUDE ========================= //

#if defined _GoVeto_VetoPlayer_included_
#endinput
#endif
#define _GoVeto_VetoPlayer_included_

// =========================================================== //

methodmap VetoPlayer < StringMap
{
	public VetoPlayer(int client)
	{
		StringMap player = new StringMap();

		if (client > 0)
		{
			char name[MAX_NAME_LENGTH];
			GetClientName(client, name, sizeof(name));

			player.SetString("name", name);
			player.SetValue("userid", GetClientUserId(client));
		}
		else
		{
			player.SetValue("userid", 0);
			player.SetString("name", "Server");
		}

		return view_as<VetoPlayer>(player);
	}
	
	property int UserId
	{
		public get()
		{
			int userid = 0;
			this.GetValue("userid", userid);
			return userid;
		}
	}
	
	property int Client
	{
		public get() { return GetClientOfUserId(this.UserId); }
	}
	
	public void GetName(char[] buffer, int maxlength)
	{
		this.GetString("name", buffer, maxlength);
	}
}

methodmap VetoPlayers < ArrayList
{
	public VetoPlayers()
	{
		return view_as<VetoPlayers>(new ArrayList());
	}
	
	public void PushClient(int client)
	{
		this.Push(new VetoPlayer(client));
	}
	
	public void Reset()
	{
		for (int i = 0; i < this.Length; i++)
		{
			VetoPlayer player = this.Get(i);
			delete player;
		}
		
		this.Clear();
	}
}

// =========================================================== //