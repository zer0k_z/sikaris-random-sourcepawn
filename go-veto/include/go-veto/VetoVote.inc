// ================== DOUBLE INCLUDE ========================= //

#if defined _GoVeto_VetoVote_included_
#endinput
#endif
#define _GoVeto_VetoVote_included_

// =========================================================== //

enum VoteType 
{
	Vote_Ban,
	Vote_Pick,
	Vote_Decider
};

// =========================================================== //

methodmap VetoVote < StringMap
{
	public VetoVote(VetoPlayer player, char[] map, VoteType type)
	{
		StringMap selection = new StringMap();
		selection.SetString("map", map);
		selection.SetValue("type", type);
		selection.SetValue("player", player);
		return view_as<VetoVote>(selection);
	}
	
	property VoteType Type
	{
		public get()
		{
			VoteType type;
			this.GetValue("type", type);
			return type;
		}
	}
	
	property VetoPlayer Player
	{
		public get()
		{
			VetoPlayer player = null;
			this.GetValue("player", player);
			return player;
		}
	}

	public void GetMap(char[] buffer, int maxlength)
	{
		this.GetString("map", buffer, maxlength);
	}
}

methodmap VetoVotes < ArrayList
{
	public VetoVotes()
	{
		return view_as<VetoVotes>(new ArrayList());
	}
	
	public void PushVote(VetoPlayer player, char[] map, VoteType type)
	{
		this.Push(new VetoVote(player, map, type));
	}
	
	public void Reset()
	{
		for (int i = 0; i < this.Length; i++)
		{
			VetoVote selection = this.Get(i);
			delete selection;
		}

		this.Clear();
	}
}

// =========================================================== //