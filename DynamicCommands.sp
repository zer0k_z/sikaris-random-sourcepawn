/*
	Allows support for dynamic commands
	This reads "cfg/sourcemod/DynamicCommands.cfg"
	in KeyValues format by default, all usable colors below

	Example config:
	"DynamicCommands"
	{
		"sm_vip"
		{
			"message"		"{darkred}Secret command for vips{default}"
			"flags"			"a"
			"description"	"Secret VIP only command"
			"runcommands"	"say {name} used the secret command; say he is cool!" 
		}
		"sm_rules"
		{
			"message"		"{darkred}You can find the rules at {blue}https://somewebsite.com/rules{default}"
			"description"	"Displays link to our rules"
		}
	}
*/

// ====================== DEFINITIONS ======================== //

//#define DEBUG

#define MAX_DYNAMIC_FLAGS_LENGTH 32
#define MAX_DYNAMIC_COMMAND_LENGTH 64
#define MAX_DYNAMIC_MESSAGE_LENGTH 512
#define MAX_DYNAMIC_DESCRIPTION_LENGTH 128
#define MAX_DYNAMIC_RUNCOMMANDS_LENGTH 1024

#define DYNAMIC_COMMANDS_CONFIG "cfg/sourcemod/DynamicCommands.cfg"

// ====================== VARIABLES ========================== //

StringMap g_Dynamics = null;

char gC_availableColors[][][] =
{
	{ "default",	"\x01" },
	{ "darkred",	"\x02" },
	{ "green",		"\x04" },
	{ "lightgreen", "\x03" },
	{ "orange",		"\x03" },
	{ "blue",		"\x03" },
	{ "olive",		"\x05" },
	{ "lime",		"\x06" },
	{ "red",		"\x07" },
	{ "purple",		"\x03" },
	{ "grey",		"\x08" },
	{ "yellow",		"\x09" },
	{ "lightblue",	"\x0A" },
	{ "steelblue",	"\x0B" },
	{ "darkblue",	"\x0C" },
	{ "pink",		"\x0E" },
	{ "lightred", 	"\x0F" }
};

// ====================== PLUGIN INFO ======================== //

public Plugin myinfo = 
{
	name = "Dynamic Commands",
	author = "Sikari",
	description = "Provides extensible support for dynamic commands",
	version = "1.0.0",
	url = ""
};

// ======================= MAIN CODE ========================= //

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	RegAdminCmd("sm_reload_dynamiccommands", Command_ReloadCommands, ADMFLAG_ROOT);
}

public void OnPluginStart()
{
	g_Dynamics = new StringMap();
	ParseDynamicCommands(DYNAMIC_COMMANDS_CONFIG);
}

public Action Command_ReloadCommands(int client, int args)
{
	ParseDynamicCommands(DYNAMIC_COMMANDS_CONFIG);
	ReplyToCommand(client, "Reloaded dynamic commands!");
}

public Action Command_Dynamic(int client, int args)
{
	if (!IsValidClient(client))
	{
		return Plugin_Handled;
	}
	
	char commandName[MAX_DYNAMIC_COMMAND_LENGTH];
	GetCmdArg(0, commandName, sizeof(commandName));

	StringMap dynamicCmd = null;
	g_Dynamics.GetValue(commandName, dynamicCmd);
	
	if (dynamicCmd != null)
	{
		char dynamicMessage[MAX_DYNAMIC_MESSAGE_LENGTH];
		dynamicCmd.GetString("message", dynamicMessage, sizeof(dynamicMessage));
	
		char dynamicRunCommands[MAX_DYNAMIC_RUNCOMMANDS_LENGTH];
		dynamicCmd.GetString("runcommands", dynamicRunCommands, sizeof(dynamicRunCommands));

		// We dont want to print empty messages
		if (!StrEqual(dynamicMessage, ""))
		{
			ReplySource source = GetCmdReplySource();
					
			if (source == SM_REPLY_TO_CHAT)
			{
				HandleMessage(client, dynamicMessage, sizeof(dynamicMessage), true);
				PrintToChat(client, " %s", dynamicMessage);
			}
			else if (source == SM_REPLY_TO_CONSOLE)
			{
				HandleMessage(client, dynamicMessage, sizeof(dynamicMessage), false);
				PrintToConsole(client, dynamicMessage);
			}
		}
			
		// We also dont want to run empty commands
		if (!StrEqual(dynamicRunCommands, ""))
		{
			DebugMessage("Raw Input: %s", dynamicRunCommands);
			HandleVariables(client, dynamicRunCommands, sizeof(dynamicRunCommands));
			DebugMessage("Running >> \"%s\"", dynamicRunCommands);
			ServerCommand(dynamicRunCommands);
		}
	}

	return Plugin_Handled;
}

// ======================= PRIVATE =========================== //

static void ParseDynamicCommands(char[] configPath)
{
	if (!FileExists(configPath))
	{
		SetFailState("%s does not exist!", configPath);
	}

	KeyValues dynamicCmds = new KeyValues("DynamicCommands");
	if (!dynamicCmds.ImportFromFile(configPath))
	{
		SetFailState("Failed reading %s as KeyValues! Make sure file is in KeyValues format", configPath);
	}
	
	g_Dynamics.Clear();
	char dynamicFlags[MAX_DYNAMIC_FLAGS_LENGTH];
	char dynamicCommand[MAX_DYNAMIC_COMMAND_LENGTH];
	char dynamicMessage[MAX_DYNAMIC_MESSAGE_LENGTH];
	char dynamicDescription[MAX_DYNAMIC_DESCRIPTION_LENGTH];
	char dynamicRunCommands[MAX_DYNAMIC_RUNCOMMANDS_LENGTH];

	while (dynamicCmds.GotoFirstSubKey() || dynamicCmds.GotoNextKey())
	{
		dynamicCmds.GetSectionName(dynamicCommand, sizeof(dynamicCommand));
		dynamicCmds.GetString("flags", dynamicFlags, sizeof(dynamicFlags));
		dynamicCmds.GetString("message", dynamicMessage, sizeof(dynamicMessage));
		dynamicCmds.GetString("description", dynamicDescription, sizeof(dynamicDescription));
		dynamicCmds.GetString("runcommands", dynamicRunCommands, sizeof(dynamicRunCommands));
		
		StringMap dynamicCmd = new StringMap();
		dynamicCmd.SetString("flags", dynamicFlags);
		dynamicCmd.SetString("command", dynamicCommand);
		dynamicCmd.SetString("message", dynamicMessage);
		dynamicCmd.SetString("description", dynamicDescription);
		dynamicCmd.SetString("runcommands", dynamicRunCommands);
		
		g_Dynamics.SetValue(dynamicCommand, dynamicCmd);

		if (!CommandExists(dynamicCommand))
		{
			int flags = ReadFlagString(dynamicFlags);
			if (flags <= 0)
			{
				RegConsoleCmd(dynamicCommand, Command_Dynamic, dynamicDescription);
			}
			else
			{
				RegAdminCmd(dynamicCommand, Command_Dynamic, flags, dynamicDescription);
			}
		}
	}
}

static void HandleMessage(int client, char[] buffer, int maxlength, bool chat)
{
	HandleColors(buffer, maxlength, !chat);
	HandleVariables(client, buffer, maxlength);
}

static void HandleVariables(int client, char[] buffer, int maxlength)
{
	HandleUserId(client, buffer, maxlength);
	HandleSteamId2(client, buffer, maxlength);
	HandleUsername(client, buffer, maxlength);
}

static void HandleColors(char[] buffer, int maxlength, bool strip = false)
{
	char searchString[16];
	int maxColors = sizeof(gC_availableColors);
	
	for (int i = 0; i < maxColors; i++)
	{
		Format(searchString, sizeof(searchString), "{%s}", gC_availableColors[i][0]);

		if (strip)
		{
			ReplaceString(buffer, maxlength, searchString, "", false);
		}
		else
		{
			ReplaceString(buffer, maxlength, searchString, gC_availableColors[i][1], false);
		}
	}
}

static void HandleUserId(int client, char[] buffer, int maxlength)
{
	char replaceString[32];
	char[] searchString = "{userid}";

	if (StrContains(buffer, searchString, false) != -1)
	{
		int userid = -1;
		if (client > 0)
		{
			userid = GetClientUserId(client);
		}

		Format(replaceString, sizeof(replaceString), "%d", userid);
		ReplaceString(buffer, maxlength, searchString, replaceString, false);
	}
}

static void HandleSteamId2(int client, char[] buffer, int maxlength)
{
	char replaceString[32];
	char[] searchString = "{steamid2}";

	if (StrContains(buffer, searchString, false) != -1)
	{
		char steamId2[32] = "Server";
		if (client > 0)
		{
			if (!GetClientAuthId(client, AuthId_Steam2, steamId2, sizeof(steamId2)))
			{
				steamId2 = "Unknown";
			}
		}
		
		Format(replaceString, sizeof(replaceString), steamId2);
		ReplaceString(buffer, maxlength, searchString, replaceString, false);
	}
}

static void HandleUsername(int client, char[] buffer, int maxlength)
{
	char replaceString[MAX_NAME_LENGTH];
	char[] searchString = "{name}";
	
	if (StrContains(buffer, searchString, false) != -1)
	{
		char clientName[MAX_NAME_LENGTH];
		Format(clientName, sizeof(clientName), "%N", client);
		ReplaceString(clientName, sizeof(clientName), ";", "", false);
		
		Format(replaceString, sizeof(replaceString), clientName);
		ReplaceString(buffer, maxlength, searchString, replaceString, false);
	}
}

// ========================= STOCKS ========================== //

stock bool IsValidClient(int client)
{
	if (client == 0) return true;
	return client >= 1 && client <= MaxClients && IsClientInGame(client) && !IsFakeClient(client);
}

stock void DebugMessage(char[] format, any ...)
{
	#if defined DEBUG

	char message[512];
	VFormat(message, sizeof(message), format, 2);
	LogMessage(message);

	#endif
}

// =========================================================== //