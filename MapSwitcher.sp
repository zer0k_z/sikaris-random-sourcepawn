// ====================== DEFINITIONS ======================== //

#define LOGFILE "logs/completed_maps.txt"

// =========================================================== //

#undef REQUIRE_PLUGIN
#include <kztimer>
#include <gokz/core>

// ====================== FORMATTING ========================= //

#pragma newdecls required

// ====================== VARIABLES ========================== //

int gI_currentMapIndex = 0;
float gF_MapTimesSum = 0.00;
ArrayList g_cachedMapList = null;
char gC_LogFile[PLATFORM_MAX_PATH];

// ======================= INCLUDES ========================== //

// ...

// ====================== PLUGIN INFO ======================== //

public Plugin myinfo = 
{
	name = "MapSwitcher",
	author = "Sikari",
	description = "",
	version = "1.0.0",
	url = ""
};

// ======================= MAIN CODE ========================= //

public void OnPluginStart()
{
	BuildPath(Path_SM, gC_LogFile, sizeof(gC_LogFile), LOGFILE);
	DeleteFile(gC_LogFile);
	
	g_cachedMapList = new ArrayList(PLATFORM_MAX_PATH);
	ReadMapList(g_cachedMapList);

	RegAdminCmd("sm_resetrun", Command_ResetRun, ADMFLAG_GENERIC);
}

public void OnMapStart()
{
	char mapName[PLATFORM_MAX_PATH];
	GetCurrentMap(mapName, sizeof(mapName));
	GetMapDisplayName(mapName, mapName, sizeof(mapName));
	
	// Hey we're here!
	int index = g_cachedMapList.FindString(mapName);
	gI_currentMapIndex = index >= 0 ? index : 0;
}

public Action Command_ResetRun(int client, int args)
{
	gF_MapTimesSum = 0.00;
	gI_currentMapIndex = 0;
	
	DeleteFile(gC_LogFile);
	ChangeMapToMapcycleIndex(gI_currentMapIndex);
}

public int KZTimer_TimerStopped(int client, int teleports, float time, int record)
{
	gF_MapTimesSum += time;
	WriteMapCompletionToFile(gI_currentMapIndex, time, teleports);
	
	if (IncrementMapcycleIndex())
	{
		ChangeMapToMapcycleIndex(gI_currentMapIndex);
	}
	else
	{
		WriteMapTimesSumToFile();
		PrintToChatAll("%N's run just finished! - Sum of times: %f (%s)", 
						client, gF_MapTimesSum, GOKZ_FormatTime(gF_MapTimesSum));
	}
}

public Action GOKZ_OnTimerEnd(int client, int course, float time, int teleportsUsed)
{
	gF_MapTimesSum += time;
	WriteMapCompletionToFile(gI_currentMapIndex, time, teleportsUsed);
	
	if (IncrementMapcycleIndex())
	{
		ChangeMapToMapcycleIndex(gI_currentMapIndex);
	}
	else
	{
		WriteMapTimesSumToFile();
		PrintToChatAll("%N's run just finished! - Sum of times: %f (%s)", 
						client, gF_MapTimesSum, GOKZ_FormatTime(gF_MapTimesSum));
	}
}

static void ChangeMapToMapcycleIndex(int mapIndex)
{
	char mapName[PLATFORM_MAX_PATH];
	g_cachedMapList.GetString(mapIndex, mapName, sizeof(mapName));
	ForceChangeLevel(mapName, "Changing to the next map in mapcycle");
}

static bool IncrementMapcycleIndex()
{
	if (gI_currentMapIndex >= g_cachedMapList.Length - 1)
	{
		return false;
	}
	else
	{
		gI_currentMapIndex++;
		return true;
	}
}

static void WriteMapTimesSumToFile()
{
	File logFile = OpenFile(gC_LogFile, "a");
	logFile.WriteLine("Run finished! - Total sum of times: %f (%s)", 
						gF_MapTimesSum, GOKZ_FormatTime(gF_MapTimesSum));
	logFile.Close();
}

static void WriteMapCompletionToFile(int mapIndex, float time, int teleports)
{
	char mapName[PLATFORM_MAX_PATH];
	g_cachedMapList.GetString(mapIndex, mapName, sizeof(mapName));
	
	File logFile = OpenFile(gC_LogFile, "a");
	logFile.WriteLine("Map: %s - Teleports: %d - Time: %f (%s)",
						mapName, teleports, time, GOKZ_FormatTime(time));
						
	logFile.Close();
}

// =========================================================== //
