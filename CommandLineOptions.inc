// ================== DOUBLE INCLUDE ========================= //

#if defined _CommandLineOptions_included_
#endinput
#endif
#define _CommandLineOptions_included_

// =========================================================== //

#include <StringMapEx>

// =========================================================== //

//#define COMMANDLINEOPTIONS_DEBUG
#define COMMANDLINEOPTIONS_VERSION "1.0.0"

#define MAX_OPTION_LENGTH 64
#define MAX_OPTION_DESC_LENGTH 128
#define MAX_OPTION_VALUE_LENGTH 128

static char OPTION_KEY[] = "option";
static char OPTION_VALUE_KEY[] = "value";
static char OPTION_IS_DEFINED_KEY[] = "is_defined";
static char OPTION_DESCRIPTION_KEY[] = "description";

// =========================================================== //

methodmap CommandLineOption < StringMapEx
{
	public CommandLineOption(char[] option, char[] description)
	{
		StringMapEx newOpt = new StringMapEx();
		newOpt.SetString(OPTION_KEY, option);
		newOpt.SetString(OPTION_VALUE_KEY, "");
		newOpt.SetValue(OPTION_IS_DEFINED_KEY, false);
		newOpt.SetString(OPTION_DESCRIPTION_KEY, description);
		
		return view_as<CommandLineOption>(newOpt);
	}
	
	public void GetName(char[] buffer, int maxlength)
	{
		this.GetString(OPTION_KEY, buffer, maxlength);
	}
	
	public void GetDesc(char[] buffer, int maxlength)
	{
		this.GetString(OPTION_DESCRIPTION_KEY, buffer, maxlength);
	}
	
	public void SetVal(char[] value)
	{
		this.SetString(OPTION_VALUE_KEY, value);
	}

	public void GetVal(char[] buffer, int maxlength)
	{
		this.GetString(OPTION_VALUE_KEY, buffer, maxlength);
	}

	property int IntVal
	{
		public get()
		{
			char buffer[MAX_OPTION_VALUE_LENGTH];
			this.GetVal(buffer, sizeof(buffer));
			return StringToInt(buffer);
		}
	}
	
	property bool BoolVal
	{
		public get()
		{
			char buffer[MAX_OPTION_VALUE_LENGTH];
			this.GetVal(buffer, sizeof(buffer));
			return !!StringToInt(buffer);
		}
	}

	property float FloatVal
	{
		public get()
		{
			char buffer[MAX_OPTION_VALUE_LENGTH];
			this.GetVal(buffer, sizeof(buffer));
			return StringToFloat(buffer);
		}
	}

	property bool IsDefined
	{
		public get() { return this.GetBool(OPTION_IS_DEFINED_KEY); }
		public set(bool isDefined) { this.SetValue(OPTION_IS_DEFINED_KEY, isDefined); }
	}
}

// =========================================================== //

methodmap CommandlineOptions < StringMapEx
{
	public CommandlineOptions(bool builtInOptions = true)
	{
		StringMapEx opts = new StringMapEx();
		
		if (builtInOptions)
		{
			opts.SetValue("--help", new CommandLineOption("--help", ""));
			opts.SetValue("--version", new CommandLineOption("--version", ""));
		}
		
		return view_as<CommandlineOptions>(opts);
	}

	public CommandLineOption GetOption(char[] option)
	{
		return this.GetAny(option);
	}

	public bool OptionExists(char[] option)
	{
		return this.GetOption(option) != null;
	}

	public void AddOption(char[] option, char[] description = "")
	{
		this.SetValue(option, new CommandLineOption(option, description));
	}
	
	property int NumDefined
	{
		public get()
		{
			int definedOptions = 0;
			StringMapSnapshot snap = this.Snapshot();
			
			for (int i = 0; i < snap.Length; i++)
			{
				char option[MAX_OPTION_LENGTH];
				snap.GetKey(i, option, sizeof(option));
				
				CommandLineOption opt = this.GetOption(option);
				
				if (opt != null && opt.IsDefined)
				{
					definedOptions++;
				}
			}
			
			delete snap;
			return definedOptions;
		}
	}
	
	public void GetHelpBuffer(char[] buffer, int maxlength)
	{
		StringMapSnapshot snap = this.Snapshot();

		char tempBuf[MAX_OPTION_LENGTH + MAX_OPTION_DESC_LENGTH + 20];
		Format(tempBuf, sizeof(tempBuf), "\n--- CLI Options ---\n");
		StrCat(buffer, maxlength, tempBuf);
		
		for (int i = 0; i < snap.Length; i++)
		{
			char option[MAX_OPTION_LENGTH];
			snap.GetKey(i, option, sizeof(option));

			CommandLineOption opt = this.GetOption(option);

			if (opt != null)
			{
				char desc[MAX_OPTION_DESC_LENGTH];
				opt.GetDesc(desc, sizeof(desc));

				Format(tempBuf, sizeof(tempBuf), " > %15s | \"%s\"\n", option, desc);
				StrCat(buffer, maxlength, tempBuf);
			}
		}
		
		Format(tempBuf, sizeof(tempBuf), "--- CLI Options ---");
		StrCat(buffer, maxlength, tempBuf);
		delete snap;
	}

	public void Parse(char[] buffer)
	{
		StringMapSnapshot snap = this.Snapshot();
		
		for (int i = 0; i < snap.Length; i++)
		{
			char option[MAX_OPTION_LENGTH];
			snap.GetKey(i, option, sizeof(option));

			#if defined COMMANDLINEOPTIONS_DEBUG
			PrintToServer("\nParsing %s", option);
			#endif
			
			CommandLineOption opt = this.GetOption(option);
			
			int index = StrContains(buffer, option);
			if (index != -1)
			{
				int delimiter = strlen(buffer) - 1;
				int valueStart = delimiter;
				
				if ((index + strlen(option)) < strlen(buffer))
				{
					valueStart = delimiter;
					delimiter = index + strlen(option);
					
					if (!IsCharSpace(buffer[delimiter]))
					{
						valueStart = delimiter;

						#if defined COMMANDLINEOPTIONS_DEBUG
						PrintToServer("[%s] Expecting a whitespace at index %d, instead found '%c'", option, delimiter, buffer[delimiter]);
						#endif
					}
					if ((delimiter + 1) <= strlen(buffer))
					{
						valueStart = delimiter + 1;
					}
				}
				
				char destBuffer[MAX_OPTION_VALUE_LENGTH];

				if (valueStart <= delimiter)
				{
					#if defined COMMANDLINEOPTIONS_DEBUG
					PrintToServer("[%s] No value", option);
					#endif
				}
				// Treat as quoted string
				else if (buffer[valueStart] == '"' || buffer[valueStart] == ''')
				{
					char quote = buffer[valueStart];
					
					// Skip the quote
					valueStart = valueStart + 1;
					
					#if defined COMMANDLINEOPTIONS_DEBUG
					PrintToServer("[%s] -> index %d ('%c')", option, valueStart, buffer[valueStart]);
					#endif
					
					int valueEnd = valueStart;
					int nextQuote = FindCharInString(buffer[valueStart], quote);
					
					if (nextQuote == -1)
					{
						valueEnd = strlen(buffer) - 1;
						
						#if defined COMMANDLINEOPTIONS_DEBUG
						PrintToServer("[%s] Expecting a quote after index %d!", option, valueStart);
						#endif
					}
					else
					{
						valueEnd = valueStart + (nextQuote - 1);
					}
					
					#if defined COMMANDLINEOPTIONS_DEBUG
					PrintToServer("[%s] <- index %d ('%c')", option, valueEnd, buffer[valueEnd]);
					#endif
	
					for (int x = valueStart; x <= valueEnd; x++)
					{
						Format(destBuffer, sizeof(destBuffer), "%s%c", destBuffer, buffer[x]);
					}
				}
				else if (buffer[valueStart] != '-' && buffer[valueStart + 1] != '-')
				{
					#if defined COMMANDLINEOPTIONS_DEBUG
					PrintToServer("[%s] -> index %d ('%c')", option, valueStart, buffer[valueStart]);
					#endif

					int valueEnd = valueStart;
					int nextSpace = FindCharInString(buffer[valueStart], ' ');
					
					if (nextSpace == -1)
					{
						valueEnd = strlen(buffer) - 1;
					}
					else
					{
						valueEnd = valueStart + (nextSpace - 1);
					}

					#if defined COMMANDLINEOPTIONS_DEBUG
					PrintToServer("[%s] <- index %d ('%c')", option, valueEnd, buffer[valueEnd]);
					#endif

					for (int x = valueStart; x <= valueEnd; x++)
					{
						Format(destBuffer, sizeof(destBuffer), "%s%c", destBuffer, buffer[x]);
					}
				}
				
				#if defined COMMANDLINEOPTIONS_DEBUG
				PrintToServer("[%s] Value: \"%s\"", option, destBuffer);
				#endif
				
				opt.SetVal(destBuffer);
				opt.IsDefined = true;
			}
			else
			{
				#if defined COMMANDLINEOPTIONS_DEBUG
				PrintToServer("[%s] Option not specified", option);
				#endif
				
				opt.SetVal("");
				opt.IsDefined = false;
			}
		}
		
		delete snap;
	}
}

// =========================================================== //